******
ROSDEP
******

.. note:: This is something custom of BOR. It is a fork of the original project in https://github.com/ros/rosdistro

***
Why
***
Rosdep is one of the infrastructural pieces of ROS. It helps to manage the dependencies on ROS packages at a ROS level and at a system level.
The original project is well maintained but is only missing one thing: our private package.
Because of it, we have a fork where we add our packages and dependencies as an extension of it.

As we only support :code:`melodic`, :code:`noetic`, and :code:`foxy` we have modified the :code:`index.yaml` and :code:`index-v4.yaml` to expose only such distros.

***
How
***

* For ROS packages, paste the definitions on top of the rosdistro file. As of today we only suport :code:`melodic`, :code:`noetic`, and :code:`foxy` so edit the :code:`melodic/distribution.yaml`, :code:`noetic/distribution.yaml`, or :code:`foxy/distribution.yaml`.
* For System dependencies, use as in the original project.

On every commit (check :code:`bitbucket-pipelines.yaml` file):

1. a sync with upstream will be done. If there are new commits, the pipeline will commit and push back to the project (thus we are always in sync)
2. the cache will be generated
3. the cache will be uploaded to the :code:`Downloads` of the repo

.. note:: This repo is publicly available to simplify things on the developers computers and robots. The only risk is that 3rd parties can get to know the name of custom ROS packages, which is considered acceptable.

***************
Troubleshooting
***************

As the pipeline is syncing with upstream automatically, there might be some merge conflicts. These have to be resolved manually by syncing the repos manually. To do so:

1. Clone this project with :code:`git clone git@bitbucket.org:blue-ocean-robotics/rosdistro.git`
2. Add the original project as a remote with :code:`git remote add original https://github.com/ros/rosdistro`
3. Fetch the original project with :code:`git fetch original master`
4. Checkout our custom branch with :code:`git checkout bor`
5. Merge with :code:`git merge original/master`
6. Fix the merge
7. Commit and push
